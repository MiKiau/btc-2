#ifndef TRANSACTION_POOL_HPP
#define TRANSACTION_POOL_HPP

#include <fstream>
#include <set>

#include "Transaction.hpp"
#include "Users.hpp"

#define DEFAULT_TRANSACTIONS_COUNT 10000
#define DEFAULT_TRANSACTIONS_FILE "data/transactions.txt"

class TransactionPool {
public:
    TransactionPool() = default;
    TransactionPool(int transactionCount);
    void fillPoolWithRandomTransactionsUsing(Users* users);
    int getValidAndUnexecutedTransactionCount();
    std::vector<Transaction*> getRandomTransactionSet(int size);
    void generateTransactionFile();

private:
    std::vector<Transaction> pool;
    int transactionCount = DEFAULT_TRANSACTIONS_COUNT;

    Transaction* getTransaction(int);
    Transaction* getRandomTransaction();
    bool isTransactionSuitable(Transaction *transaction);
    bool isTransactionInAVector(Transaction *transaction, std::vector<Transaction*> *transactions);
};

#endif // TRANSACTION_POOL_HPP