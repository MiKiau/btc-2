#include "Random.hpp"


int Random::getRandomIntegerInRange(const int min, const int max) {
    std::random_device rd;
    std::mt19937 mt(rd());
    std::uniform_int_distribution<int> uid(min, max);
    return uid(mt);
}

double Random::getRandomDoubleInRange(const double min, const double max) {
    std::random_device rd;
    std::mt19937 mt(rd());
    std::uniform_real_distribution<double> urd(min, max);
    return floor((urd(mt) * 100) + 0.5) / 100.0;
}