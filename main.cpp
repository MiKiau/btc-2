#include <iostream>
#include <thread>
#include <atomic>

#include "includes/Users.hpp"
#include "includes/TransactionPool.hpp"
#include "includes/BlockChain.hpp"
#include "includes/Block.hpp"

#define MAX_NUM_OF_MINING_TRIES 100000

int numberInputFromConsole(std::string message);
bool isInteger(const std::string word);
void task(std::atomic_bool &mined, Block &block);
void startParallelMining(int minersCount, TransactionPool *pool, BlockChain *blockChain);

int main() {
    std::cout << "\nPlease input parameters: \n";
    int userCount = numberInputFromConsole("Users count: ");
    int transactionCount = numberInputFromConsole("Transaction count: ");
    
    Users users(userCount);
    TransactionPool pool(transactionCount);

    std::cout << "\nFilling the users and transaction pool with random data.\n";
    users.generateUsers();
    pool.fillPoolWithRandomTransactionsUsing(&users);
    std::cout << "----- Complete.\n";

    std::cout << "\nSaving generated data to data folder.\n";
    users.generateUserFile();
    pool.generateTransactionFile();
    std::cout << "----- Complete.\n";

    std::cout << "\nData setup complete!\n";

    std::cout << "\nStarting block mining and updating blockchain.\n";
    BlockChain blockChain;

    while (pool.getValidAndUnexecutedTransactionCount() > 0) {
        startParallelMining(5, &pool, &blockChain);
    }
    std::cout << "----- Mining complete.\n";

    std::cout << "\nSaving blockchain to data folder.\n";
    blockChain.generateInfoFiles();
    std::cout << "----- Complete.\n";

    std::cout << "\n\nThe program finished running successfully.\n\n";
    return 0;
}

int numberInputFromConsole(std::string message) {
    std::cout << message;

    std::string line = "";
    do {
        std::getline(std::cin, line);
        if (isInteger(line)) {
            break;
        } else {
            std::cout << "Please try again: ";
        }
    } while(true);

    return std::stoi(line);
}

bool isInteger(const std::string text) {
    if (text.empty() || (!isdigit(text[0]) && (text[0] != '-') && (text[0] != '+'))) {
        return false;
    }

    char *p;
    // Traverse the text string until 10th character or until the first not digit
    strtol(text.c_str(), &p, 10);

    // if the p points to the last symbol (\0), then true. Else false.
    return (*p == 0);
}

void task(std::atomic_bool &mined, Block &block) {
    if (block.threadSafeMine(&mined)) {
        mined.store(true);
    }
}

void startParallelMining(int minersCount, TransactionPool *pool, BlockChain *blockChain) {
    std::vector<std::thread> miners;
    std::vector<Block> blocks;

    std::atomic_bool mined(false);

    for (int i = 0; i < minersCount; i++) {
        Block block;
        block.addTransactions(pool);

        miners.push_back(std::thread(task, std::ref(mined), std::ref(block)));
        blocks.push_back(block);
    }

    for (int i = 0; i < miners.size(); i++) {
        miners.at(i).join();
    }

    for (int i = 0; i < blocks.size(); i++) {
        if (blocks.at(i).isMined()) {
            if (blockChain->blocks.size() > 0) {
                blocks.at(i).setPrevHash(blockChain->blocks.back().generateHash());
            }
            blocks.at(i).updateMerkleRoot();
            blocks.at(i).executeTransactions();
            blockChain->addBlock(blocks.at(i));

            std::cout   << "Block " << blockChain->size() 
                        << " has been mined by miner " << (i + 1) << ".\n";
        }   
    }
}