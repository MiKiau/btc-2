#ifndef USERS_HPP
#define USERS_HPP

#include <fstream>
#include <string>
#include <iostream>

#include "Random.hpp"
#include "User.hpp"

#define DEFAULT_USERS_COUNT 1000
#define DEFAULT_USERS_FILE "data/users.txt"

class Users {
public:
    Users() = default;
    Users(int);
    void generateUserFile();
    User* getRandomUser();
    User* getRandomUserExcluding(User*);
    void generateUsers();

private:
    std::vector<User> users;
    int usersCount = DEFAULT_USERS_COUNT;
};

#endif // USERS_HPP