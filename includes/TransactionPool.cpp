#include "TransactionPool.hpp"

TransactionPool::TransactionPool(int transactionCount) {
    this->transactionCount = transactionCount;
}

void TransactionPool::fillPoolWithRandomTransactionsUsing(Users* users) {
    for (int i = 0; i < this->transactionCount; i++) {
        User* sender = users->getRandomUser();
        User* receiver = users->getRandomUserExcluding(sender);
        this->pool.push_back(Transaction(sender, receiver));
    }
}

int TransactionPool::getValidAndUnexecutedTransactionCount() {
    int result = 0;
    for (auto transaction : this->pool) {
        result += (!transaction.isExecuted() && transaction.isValid()) ? 1 : 0;
    }
    return result;
}

Transaction* TransactionPool::getTransaction(int index) {
    return &this->pool.at(index);
}

Transaction* TransactionPool::getRandomTransaction() {
    int index = Random::getRandomIntegerInRange(0, this->transactionCount - 1);
    return this->getTransaction(index);
}

void TransactionPool::generateTransactionFile() {
    std::ofstream fileData(DEFAULT_TRANSACTIONS_FILE);

    for (auto transaction : this->pool) {
        fileData << transaction.toString();
    }
    
    fileData.close();
}

bool TransactionPool::isTransactionSuitable(Transaction *transaction) {
    return  !transaction->isExecuted() 
            && transaction->isValid()
            && transaction->generateFieldsHash() == transaction->getTransactionId();
}

bool TransactionPool::isTransactionInAVector(Transaction *transaction, std::vector<Transaction*> *transactions) {
    for (int i = 0; i < transactions->size(); i++) {
        if (transaction->getTransactionId() == transactions->at(i)->getTransactionId()) {
            return true;
        }
    }
    return false;
}

std::vector<Transaction*> TransactionPool::getRandomTransactionSet(int size) {
    std::vector<Transaction*> transactions;
    int finalSize = std::min(size, this->getValidAndUnexecutedTransactionCount());
    
    while (transactions.size() < finalSize) {
        Transaction *transaction = this->getRandomTransaction();
        if (this->isTransactionSuitable(transaction) && !this->isTransactionInAVector(transaction, &transactions)) {
            transactions.push_back(transaction);
        }
    }

    return transactions;
}