#include "BlockChain.hpp"

void BlockChain::addBlock(const Block& block) {
    this->blocks.push_back(block);
}

bool BlockChain::isEmpty() {
    return this->blocks.empty();
}

int BlockChain::size() {
    return this->blocks.size();
}

void BlockChain::generateInfoFiles() {
    for (int i = 0; i < this->blocks.size(); i++) {
        this->generateBlockInfoFile(&this->blocks.at(i), i);
    }
}

void BlockChain::generateBlockInfoFile(Block* block, int blockIndex) {
    std::string filePath = DEFAULT_BLOCK_INFO_FOLDER + std::to_string(blockIndex) + ".txt";
    std::ofstream fileData(filePath);
    fileData << block->toString();    
    fileData.close();
}