#include "User.hpp"

User::User(std::string name) {
    this->name = name;
    this->balance = Random::getRandomDoubleInRange(MIN_USER_BALANCE, MAX_USER_BALANCE);
    this->publicKey = Hash::hash(this->name + " " + std::to_string(this->balance));
}

std::string User::getName() {
    return this->name;
}

std::string User::getPublicKey() {
    return this->publicKey;
}

bool User::isWithdrawalPossible(double amount) {
    return this->balance >= amount;
}

void User::withdraw(double amount) {
    if (this->isWithdrawalPossible(amount)) {
        this->balance -= amount;
    }
}

void User::deposit(double amount) {
    this->balance += amount;
}

std::string User::toString() {
    return this->name + " " +
        this->publicKey + " " +
        std::to_string(this->balance)  + '\n';
}