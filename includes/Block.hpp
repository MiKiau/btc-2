#ifndef BLOCK_HPP
#define BLOCK_HPP

#include <string>
#include <sstream>
#include <chrono>
#include <atomic>

#include "Transaction.hpp"
#include "TransactionPool.hpp"
#include "Hash.hpp"
#include "MerkleRoot.hpp"

#define MAX_TRANSACTION_COUNT 100

class Block {
public:
    std::string prevBlockHash;
    long timeStamp;
    int version = 1.0;
    std::string merkelRootHash = "HASH";
    int nonce = 0;
    int difficultyTarget = 2;

    Block();
    bool isMined();
    void setPrevHash(std::string hash);
    void addTransactions(TransactionPool *pool);
    void updateMerkleRoot();
    void executeTransactions();
    bool mine(int numberOfTries);
    bool threadSafeMine(std::atomic_bool *mined);
    std::string generateHash();
    std::string toString();

private:
    bool mined;

    std::vector<Transaction*> transactions;
    int maxTransactionCount = MAX_TRANSACTION_COUNT;
};

#endif //BLOCK_HPP