# Simple BlockChain 0.2v

VU pasirenkamo kurso antra užduotis: sukurti supaprastintą blokų grandinę. Versija 0.2

## Naudojimosi instrukcijos

- VS Code aplinkoje sucompiliuoti main.cpp failą naudojant ctrl+shift+b komanda;
- Paleisti programą konsolėje naudojant ./main.exe komanda;
- Pasileidus programai konsolėje suvesti kiek bus vartotojų (users) ir kiek tranzakcijų (transactions).