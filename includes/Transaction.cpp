#include "Transaction.hpp"

Transaction::Transaction(User* sender, User* receiver) {
    this->sender = sender;
    this->receiver = receiver;
    this->payment = this->getRandomPayment();
    this->creationTimeStamp = this->getTimeStamp();
    this->transactionId = this->generateFieldsHash();

    this->executed = false;
    this->valid = this->sender->isWithdrawalPossible(this->payment);
}

std::string Transaction::getTransactionId() {
    return this->transactionId;
}

bool Transaction::isExecuted() {
    return this->executed;
}

bool Transaction::isValid() {
    return this->valid;
}

void Transaction::execute() {
    if (!this->isExecuted() && this->isValid()) {
        this->sender->withdraw(this->payment);
        this->receiver->deposit(this->payment);
        this->executed = true;
    }
}

std::string Transaction::getTimeStamp() {
    using namespace std::chrono;
    auto nowMs = time_point_cast<milliseconds>(system_clock::now());
    long time = nowMs.time_since_epoch().count();
    return std::to_string(time);
}

double Transaction::getRandomPayment() {
    return Random::getRandomDoubleInRange(MIN_USER_BALANCE, MAX_USER_BALANCE - 1);
}

std::string Transaction::generateFieldsHash() {
    std::stringstream ss;

    ss  << this->sender->getPublicKey()
        << this->receiver->getPublicKey()
        << this->payment
        << this->creationTimeStamp;
    
    return Hash::hash(ss.str());
}

std::string Transaction::toString() {
    return this->transactionId + " " +
            this->sender->getPublicKey() + " " +
            this->receiver->getPublicKey() + " " +
            std::to_string(this->payment) + '\n';
}

std::string Transaction::toStringBeautiful() {
    std::stringstream ss;
    
    ss  << "--------------------------------------------------------------------------------------\n"
        << "Transaction ID: "           << this->transactionId              << "\n"
        << "Sender info\n"
        << "\tSender: "                 << this->sender->getName()          << "\n"
        << "\tSender public key: "      << this->sender->getPublicKey()     << "\n"
        << "Receiver info\n"
        << "\tReceiver: "               << this->receiver->getName()        << "\n"
        << "\tReceiver public key: "    << this->receiver->getPublicKey()   << "\n"
        << "Amount: "                   << this->payment                    << "\n"
        << "--------------------------------------------------------------------------------------\n";

    return ss.str();
}