#include "Block.hpp"

Block::Block() {
    using namespace std::chrono;
    this->timeStamp = duration_cast<milliseconds>(system_clock::now().time_since_epoch()).count();
    this->mined = false;
}

bool Block::isMined() {
    return this->mined;
}

void Block::setPrevHash(std::string hash) {
    this->prevBlockHash = hash;
}

void Block::addTransactions(TransactionPool *pool) {
    if (this->transactions.size() == 0) {
        this->transactions = pool->getRandomTransactionSet(this->maxTransactionCount);
    }
}

void Block::updateMerkleRoot() {
    this->merkelRootHash = MerkleRoot::getRoot(this->transactions);
}

void Block::executeTransactions() {
    for (auto transaction : this->transactions) {
        transaction->execute();
    }
}

bool Block::mine(int numberOfTries) {
    std::string beginingToAim(this->difficultyTarget, '0');

    int tryCount = 0;
    while (this->generateHash().substr(0, this->difficultyTarget) != beginingToAim) {
        this->nonce++;
        tryCount++;

        if (tryCount >= numberOfTries) {
            return false;
        }
    }
    return true;
}

bool Block::threadSafeMine(std::atomic_bool *mined) {
    std::string beginingToAim(this->difficultyTarget, '0');

    while (this->generateHash().substr(0, this->difficultyTarget) != beginingToAim) {
        this->nonce++;

        if (!mined->load()) {
            return false;
        }
    }
    
    this->mined = true;
    return true;
}

std::string Block::generateHash() {
    return Hash::hash(
        this->prevBlockHash +
        std::to_string(this->timeStamp) +
        std::to_string(this->version) +
        this->merkelRootHash +
        std::to_string(this->nonce) +
        std::to_string(this->difficultyTarget)
    );
}

std::string Block::toString() {
    std::stringstream ss;

    ss  << "Previous block hash: "  << this->prevBlockHash      << "\n"
        << "Timestamp: "            << this->timeStamp          << "\n"
        << "Version: "              << this->version            << "\n"
        << "Merkle root hash: "     << this->merkelRootHash     << "\n"
        << "Nounce: "               << this->nonce              << "\n"
        << "Difficulty target: "    << this->difficultyTarget   << "\n";
    
    ss  << "\n\nThese transactions were mined in this block:\n";
    for (auto transaction : this->transactions) {
        ss << transaction->toStringBeautiful();
    }

    return ss.str();
}