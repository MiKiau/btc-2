#ifndef MERKLE_HPP
#define MERKLE_HPP

#include <string>
#include <sstream>
#include <vector>

#include "Hash.hpp"
#include "Transaction.hpp"

namespace MerkleRoot {
    std::string getRoot(std::vector<Transaction*> transactions);
};

#endif //MERKLE_HPP