#ifndef TRANSACTION_HPP
#define TRANSACTION_HPP

#include <iostream>
#include <string>
#include <sstream>
#include <chrono>

#include "Hash.hpp"
#include "User.hpp"

class Transaction {
public:
    Transaction(User*, User*);
    std::string getTransactionId();
    bool isExecuted();
    bool isValid();
    void execute();
    std::string generateFieldsHash();
    std::string toString();
    std::string toStringBeautiful();

private:
    std::string transactionId;
    User* sender;
    User* receiver;
    double payment;

    std::string creationTimeStamp;
    bool executed;
    bool valid;

    std::string getTimeStamp();
    double getRandomPayment();
};

#endif // TRANSACTION_HPP