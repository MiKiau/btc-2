#ifndef RND
#define RND

#include <random>

namespace Random {
    int getRandomIntegerInRange(const int min, const int max);
    double getRandomDoubleInRange(const double min, const double max);
}

#endif //RND