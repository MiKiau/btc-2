#ifndef BLOCK_CHAIN_HPP
#define BLOCK_CHAIN_HPP

#include <vector>

#include "Block.hpp"

#define DEFAULT_BLOCK_INFO_FOLDER "data/blocks/"

class BlockChain {
public:
    std::vector<Block> blocks;

    void addBlock(const Block&);
    bool isEmpty();
    int size();
    void generateInfoFiles();

private:
    void generateBlockInfoFile(Block* block, int blockIndex);
};


#endif //BLOCK_CHAIN_HPP