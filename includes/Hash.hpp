#ifndef HASH_HPP
#define HASH_HPP

#include "string"

namespace Hash {
    std::string hash(std::string text);
}

#endif