#include "MerkleRoot.hpp"

std::string MerkleRoot::getRoot(std::vector<Transaction*> transactions) {
    if (transactions.empty()) {
        return Hash::hash(" ");
    } else if (transactions.size() == 1) {
        return transactions.front()->getTransactionId();
    }

    std::vector<std::string> IDs;
    for (int i = 0; i < transactions.size(); i++) {
        IDs.push_back(transactions.at(i)->getTransactionId());
    }

    while (IDs.size() > 1) {
        if (IDs.size() % 2 == 1) {
            IDs.push_back(IDs.back());
        }

        std::vector<std::string> newIDs;
        for (int i = 0; i < IDs.size(); i += 2) {
            std::string concat = IDs.at(i) + IDs.at(i + 1);
            newIDs.push_back(Hash::hash(concat));
        }

        IDs = newIDs;
    }

    return IDs.front();
}