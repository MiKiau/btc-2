#include "Hash.hpp"

std::string Hash::hash(std::string text) {
    std::string symbolTable = "abcdef0123456789";
    std::string result = "9ef18054cd817d43230b3644ccb69355c44ba372c49b81dacb6fa28ebb391e74";
    int a = 1295549,
        b = 37087,
        size = result.length();
    for (auto symbol : text) {
        for (int i = 0; i < size; i++) {
            int number = result[i] * (a + b) + symbol;
            number -= (i == 0) ? result[size - 1] : result[i - 1];
            a = a * b + symbol;
            if (a < 0) {
                a = -a * 1295;
            }
            result[i] = symbolTable[number % symbolTable.length()];
        }
    }
    return result;
}