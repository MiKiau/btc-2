#ifndef USER_HPP
#define USER_HPP

#include <string>

#include "Hash.hpp"
#include "Random.hpp"

#define MIN_USER_BALANCE 100
#define MAX_USER_BALANCE 1000000

class User {
private:
    std::string name;
    std::string publicKey;
    double balance;

public:
    User() = default;
    User(std::string);
    std::string getName();
    std::string getPublicKey();
    bool isWithdrawalPossible(double);
    void withdraw(double);
    void deposit(double);
    std::string toString();
};

#endif // USER_HPP