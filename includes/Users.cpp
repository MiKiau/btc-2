#include "Users.hpp"

Users::Users(int userCount) {
    this->usersCount = userCount;
}

void Users::generateUsers() {
    for (int i = 0; i < this->usersCount; i++)
    {
        // User user("User " + std::to_string(i + 1));
        this->users.push_back(User("User" + std::to_string(i + 1)));
    }
}

void Users::generateUserFile() {
    std::ofstream fileData(DEFAULT_USERS_FILE);

    for (auto user : this->users) {
        fileData << user.toString();
    }
    
    fileData.close();
}

User* Users::getRandomUser() {
    int userIndex = Random::getRandomIntegerInRange(0, this->usersCount - 1);
    return &this->users.at(userIndex);
}

User* Users::getRandomUserExcluding(User* excludedUser) {
    User* user = this->getRandomUser();
    while (user->getName().compare(excludedUser->getName()) == 0) {
        user = this->getRandomUser();
    }
    return user;
}
